# Ensure that the custom-build-command can at least make it to the
# $module->buildInternal() portion when no build system can be auto-detected.

import unittest
from promise import Promise
# use Test::More;
# use POSIX;
# use File::Basename;
#
from ksblib.Application import Application
from ksblib.Module.Module import Module
from ksblib.BuildSystem.BuildSystem import BuildSystem
from ksblib.Debug import Debug

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


# Mock override
def mock_update(self,  *args, **kwargs):
    unittest.TestCase().assertEqual(str(self), self.name, "We're a real ksb::Module")
    unittest.TestCase().assertFalse(self.pretending(), "Test makes no sense if we're pretending")
    return 0  # shell semantics


# Mock override
def mock_install(self):
    return False


Module.update = mock_update
Module.install = mock_install


BuildSystem.testSucceeded = 0


# Mock override
def mock_buildInternal(self, *args, **kwargs):
    unittest.TestCase().assertEqual(self.name(), "generic", "custom-build-system is generic unless overridden")
    BuildSystem.testSucceeded = 1
    
    return {"was_successful": 1}


# Mock override
def mock_needsRefreshed(self):
    return ""


# Mock override
def mock_createBuildSystem(self):
    return Promise.resolve(1)


# Mock override
def mock_configureInternal(self):
    return 1


BuildSystem.buildInternal = mock_buildInternal
BuildSystem.needsRefreshed = mock_needsRefreshed
BuildSystem.createBuildSystem = mock_createBuildSystem
BuildSystem.configureInternal = mock_configureInternal


class TestApp(unittest.TestCase):
    def test1(self):
        args = "--pretend --rc-file tests/integration/fixtures/sample-rc/kdesrc-buildrc --no-metadata --custom-build-command echo --override-build-system generic".split(" ")
        
        app = Application(args)
        moduleList = app.modules
        
        self.assertEqual(len(moduleList), 4, "Right number of modules")
        self.assertEqual(moduleList[0].name, "setmod1", "mod list[0] == setmod1")
        
        module = moduleList[0]
        self.assertEqual(module.getOption("custom-build-command"), "echo", "Custom build command setup")
        self.assertEqual(module.getOption("override-build-system"), "generic", "Custom build system required")
        
        self.assertTrue(module.buildSystem() is not None, "module has a buildsystem")
        
        # Don't use ->isa because we want this exact class
        self.assertIsInstance(module.buildSystem(), BuildSystem)
        
        # Disable --pretend mode, the build/install methods should be mocked and
        # harmless and we won't proceed to buildInternal if in pretend mode
        # otherwise.
        Debug().setPretending(False)
        module.build()
        
        self.assertEqual(BuildSystem.testSucceeded, 1, "Made it to buildInternal()")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
